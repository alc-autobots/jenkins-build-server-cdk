#!/usr/bin/env python3

from aws_cdk import core

from provision_jenkins.provision_jenkins_stack import ProvisionJenkinsStack

app = core.App()
_accountID = app.node.try_get_context("accountID")
_region = app.node.try_get_context("region")
evn_cn = core.Environment(account=_accountID, region=_region)

ProvisionJenkinsStack(app, "provision-jenkins", env=evn_cn)
app.synth()
