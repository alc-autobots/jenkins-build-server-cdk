#!/bin/bash

apt-get update -y

#install java
apt-get install -y openjdk-11-jdk

#install maven
apt-get install -y maven

apt-get install -y git

#install Jenkins
wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | apt-key add -
sh -c 'echo deb https://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'
apt-get update
apt-get install -y jenkins

echo "***** STEP 1: Uninstall / remove existing doker ..."
apt-get remove docker docker-engine docker.io containerd runc


echo "***** STEP 2: apt-get update ...."
apt-get update

echo "***** STEP 3 & 4: apt-get install -y ca-cert and stuff & curl...."
apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common


echo "***** STEP 5: Add Docker’s official GPG key ...."
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -


echo "***** STEP 6: apt-key fingerprint ...."
apt-key fingerprint 0EBFCD88


echo "***** STEP 7: add-aot-repository ...."
add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"


echo "***** STEP 8: apt-get update"
apt-get update


echo "STEP 9: apt-get install docker..."
apt-get install -y docker-ce docker-ce-cli containerd.io

#Adding this line to avoid the below error due to aws :
# docker login --username AWS --password-stdin <AWS-ACCOUNT-ID>.dkr.ecr.<REGION>.amazonaws.com
#Got permission denied while trying to connect to the Docker daemon socket at unix:///var/run/docker.sock:
echo "STEP 9.a: chmod the docker.sock"
chmod 666 /var/run/docker.sock

#install aws-cli v2
echo "STEP 10: Install unzip pacakge"
apt install unzip


echo "STEP 11: Install aws-cli:v2"
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
./aws/install



#Install Node.js v15.x
# Using Ubuntu: https://github.com/nodesource/distributions/blob/master/README.md#debinstall
#curl -sL https://deb.nodesource.com/setup_15.x | sudo -E bash -
#sudo apt-get install -y nodejs

#Install Node.js v15.x (without sudo)
curl -sL https://deb.nodesource.com/setup_15.x | -E bash -
apt-get install -y nodejs