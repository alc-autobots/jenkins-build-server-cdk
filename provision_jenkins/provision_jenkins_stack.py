from aws_cdk import (core, aws_ec2 as _ec2)


class ProvisionJenkinsStack(core.Stack):

    def __init__(self, scope: core.Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        # The code that defines your stack goes here
        print("Starting...")
        try:
            with open("provision_jenkins/userdata.sh", mode="r") as f:
                user_data = f.read();
                _pem_key_name = self.node.try_get_context("pemKeyName")
                _region = self.node.try_get_context("region")
                linux_ubuntu_ami_id = self.node.try_get_context("linuxUbuntu20.04");
                _vpc_id = self.node.try_get_context("vpcID")
                _vpc = _ec2.Vpc.from_lookup(self, "VPC", vpc_id=_vpc_id)
                _security_group_id = self.node.try_get_context("securityGroupID")
                _security_group = _ec2.SecurityGroup.from_security_group_id(self,
                                                                            id="jenkinsSecurityGroupID",
                                                                            security_group_id=_security_group_id)
                # linux-ubuntu-20.04
                linux_ubuntu_image = _ec2.GenericLinuxImage({
                    "us-east-1": linux_ubuntu_ami_id
                })

                _jenkins_build_server_instance = _ec2.Instance(self,
                                                               id="jenkins_build_server",
                                                               instance_type=_ec2.InstanceType(
                                                                   instance_type_identifier="t2.medium"),
                                                               machine_image=linux_ubuntu_image,
                                                               instance_name="jenkins_build_server",
                                                               security_group=_security_group,
                                                               vpc=_vpc,
                                                               key_name=_pem_key_name,
                                                               vpc_subnets=_ec2.SubnetSelection(
                                                                   subnet_type=_ec2.SubnetType.PUBLIC),
                                                               user_data=_ec2.UserData.custom(user_data))
        except OSError as error:
            print("cannot read userdata.sh file")
            print(error)
